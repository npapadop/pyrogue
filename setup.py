#!/usr/bin/env python

from distutils.core import setup

# from distutils.extension import Extension
# from Cython.Build import cythonize

# ext_modules = [
#     Extension(
#         'c_funcs',
#         ['crime/wrappers.pyx'],
#         extra_compile_args=['-fopenmp'],
#         extra_link_args=['-fopenmp'],
#     )
# ]

setup(
    name="pyrogue",
    version="0.1",
    packages=["pyrogue"],
    # ext_package='crime',
    # ext_modules=cythonize(ext_modules),
)
