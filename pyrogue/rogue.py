import multiprocessing
from joblib import Parallel, delayed

import numpy as np
from numpy import random
import scipy as sp
import scipy.stats
import pandas as pd

from statsmodels.nonparametric.smoothers_lowess import lowess
import statsmodels.stats.multitest as multi
import bottleneck as bn


def entropy(expr, r=1):
    """
    Calculates the average expression and expression entropy of each gene in the input dataset.

    Parameters
    ----------
    expr: pandas.DataFrame
        The expression matrix. Rows should be genes and columns should be cells.
    r: float, optional
        A (fractional) pseudocount to be added to the gene averages so as to avoid log(0).
        (default: 1)

    Returns
    -------
    result: pandas.DataFrame
        A data frame with three columns, 'Gene', 'mean.expr' and 'entropy'.
    """
    tmp = np.log(expr + 1)
    ent = np.mean(tmp, axis=1)
    mean_expr = np.log(np.mean(expr, axis=1) + r)
    return pd.DataFrame({"mean.expr": mean_expr, "entropy": ent})


def _update_fit(ent_res, tmp, span=0.5):
    """
    Helper function for `entropy_fit`. Performs the entropy/average fit for the genes above a 
    p-value cutoff of 0.1.

    Parameters
    ----------
    ent_res: pandas.DataFrame
        A data frame returned by `rogue.entropy()`. Contains three columns, 'Gene', 'mean.expr' and
        'entropy'.
    tmp: pandas.DataFrame
        A data frame like `ent_res` that also contains the difference in entropy, p-value, and
        adjusted p-value for each gene.
    span: float
        Between 0 and 1. The fraction of the data used when estimating y-values in the lowess fit
        (controls the degree of smoothing).
    
    Returns
    -------
    tmp: pandas.DataFrame
        A data frame of the same shape as `ent_res`, but with updated values after the fitting.
    """
    if ent_res is None or len(ent_res) < 2:
        return None
    keep = tmp["pv"] > 0.1
    fit = lowess(tmp[keep]["entropy"], tmp[keep]["mean.expr"], frac=span)
    lowess_x = fit[:, 0]
    lowess_y = fit[:, 1]

    prd = np.interp(ent_res["mean.expr"], lowess_x, lowess_y)

    ds = prd - ent_res["entropy"]
    norm = sp.stats.norm(np.mean(ds), np.std(ds))
    pv = 1 - norm.cdf(ds)

    tmp["fit"] = prd
    tmp["ds"] = ds
    tmp["pv"] = pv
    return tmp


def entropy_fit(ent_res, span=0.5, mt_method="fdr_bh"):
    """
    Fit the relationship between expression entropy and average gene expression.

    Parameters
    ----------
    ent_res: pandas.DataFrame
        A data frame returned by `rogue.entropy()`. Contains three columns, 'Gene', 'mean.expr' and
        'entropy'.        
    span: float
        Between 0 and 1. The fraction of the data used when estimating y-values in the lowess fit
        (controls the degree of smoothing).
    mt_method: str
        Method used for testing and adjustment of pvalues. Can be either the full name or initial
        letters. Available methods are:

        - `bonferroni` : one-step correction
        - `sidak` : one-step correction
        - `holm-sidak` : step down method using Sidak adjustments
        - `holm` : step-down method using Bonferroni adjustments
        - `simes-hochberg` : step-up method  (independent)
        - `hommel` : closed method based on Simes tests (non-negative)
        - `fdr_bh` : Benjamini/Hochberg  (non-negative)
        - `fdr_by` : Benjamini/Yekutieli (negative)
        - `fdr_tsbh` : two stage fdr correction (non-negative)
        - `fdr_tsbky` : two stage fdr correction (non-negative)

        ROGUE (in R) offers `fdr` and `BH`. The `fdr_bh` option produces results identical to
        `fdr`. (default: `fdr_bh`)
    
    Returns
    -------
    tmp: pandas.DataFrame
        An updated data frame with seven columns: 'Gene', 'mean.expr', 'entropy', 'fit', 'ds', 'pv',
        and 'p.adj'.

    """
    # keep finite values for the fit
    # .x <- .x %>% dplyr::filter(is.finite(mean.expr)) %>% dplyr::filter(entropy > 0)
    ent_res = ent_res.drop_duplicates()
    keep = (np.isfinite(ent_res["mean.expr"])) & (ent_res["entropy"] > 0)
    if np.sum(keep) < 3:
        return None
    ent_res = ent_res[keep]
    # first fit the data entropy and the average expression
    prd = lowess(
        ent_res["entropy"], ent_res["mean.expr"], frac=span, return_sorted=False,
    )
    ds = prd - ent_res["entropy"]
    norm = sp.stats.norm(np.mean(ds), np.std(ds))
    pv = 1 - norm.cdf(ds)
    d = {
        "mean.expr": ent_res["mean.expr"],
        "entropy": ent_res["entropy"],
        "fit": prd,
        "ds": ds,
        "pv": pv,
    }

    # now update the fit with only the relevant results (pv>0.1)
    tmp = pd.DataFrame(d)
    tmp = _update_fit(ent_res, tmp, span=span)
    tmp = _update_fit(ent_res, tmp, span=span)

    if tmp is None:
        return None

    # multiple testing correction of the p-values
    _reject, pvals_corrected, _alpha_sidac, _alpha_bonf = multi.multipletests(
        tmp["pv"], method=mt_method
    )
    tmp["p.adj"] = pvals_corrected

    # some housekeeping:
    del d, _reject, _alpha_bonf, _alpha_sidac

    return tmp


def remove_outlier_cells(
    ent, expr, n=2, span=0.5, r=1, mt_method="fdr_bh", pval_cutoff=0.05
):
    """
    Remove outlier cells before calculating ROGUE score.

    Parameters
    ----------
    ent: pandas.DataFrame
        A data frame returned by `rogue.entropy()`. Contains three columns, 'Gene', 'mean.expr' and
        'entropy'.
    expr: pandas.DataFrame
        The expression matrix. Rows should be genes and columns should be cells.
    n: int
        The number of outlier cells with highest expression to remove (default: 2)
    span: float
        Between 0 and 1. The fraction of the data used when estimating y-values in the lowess fit
        (controls the degree of smoothing).
    r: float, optional
        A (fractional) pseudocount to be added to the gene averages so as to avoid log(0).
        (default: 1)
    mt_method: str
        Method used for testing and adjustment of pvalues. Can be either the full name or initial
        letters. Available methods are:

        - `bonferroni` : one-step correction
        - `sidak` : one-step correction
        - `holm-sidak` : step down method using Sidak adjustments
        - `holm` : step-down method using Bonferroni adjustments
        - `simes-hochberg` : step-up method  (independent)
        - `hommel` : closed method based on Simes tests (non-negative)
        - `fdr_bh` : Benjamini/Hochberg  (non-negative)
        - `fdr_by` : Benjamini/Yekutieli (negative)
        - `fdr_tsbh` : two stage fdr correction (non-negative)
        - `fdr_tsbky` : two stage fdr correction (non-negative)

        ROGUE (in R) offers `fdr` and `BH`. The `fdr_bh` option produces results identical to
        `fdr`. (default: `fdr_bh`)
    
    Returns
    -------
    tmp: pandas.DataFrame
        An updated data frame with seven columns: 'Gene', 'mean.expr', 'entropy', 'fit', 'ds', 'pv',
        and 'p.adj'.
    """
    # find minimum avg. expr.
    mean_cut = np.min(ent["mean.expr"])
    # find significant genes and subset the data
    sig_genes_loc = ent["p.adj"] < pval_cutoff
    sig_genes = np.array(ent.index[sig_genes_loc])
    if len(sig_genes) < 5:
        return None
    expr_tmp = expr.T[sig_genes].T
    # remove outliers (n cells with highest expr.)
    rm_max_2 = -bn.partition(-expr_tmp, n, axis=1)[:, n:]
    # calculate log of avg. expr and avg. entropy
    mean_v = np.log(np.mean(rm_max_2, axis=1) + r)
    entr_v = np.mean(np.log(rm_max_2 + 1), axis=1)
    # replace the significant gene expression, entropy with their new averages
    ent.loc[sig_genes_loc, "mean.expr"] = mean_v
    ent.loc[sig_genes_loc, "entropy"] = entr_v
    keep = np.zeros(ent.shape[0], dtype=bool)
    keep[ent["mean.expr"] > mean_cut] = True
    # recalculate entropy fit
    res = entropy_fit(ent[keep], span=span, mt_method=mt_method)
    return res


def calculate_rogue(ent, k, cutoff=0.05, features=None):
    if features is None:  # use all genes for the calculation
        keep = (ent["p.adj"] < cutoff) & (ent["pv"] < cutoff)
        sig_value = np.abs(ent["ds"][keep])
        sig_value = np.sum(sig_value)
        rogue = 1 - sig_value / (sig_value + k)
        return rogue
    else:
        keep = (
            (ent["p.adj"] < cutoff) & (ent["pv"] < cutoff) & (ent.index.isin(features))
        )
        sig_value = np.abs(ent["ds"][keep])
        sig_value = np.sum(sig_value)
        rogue = 1 - sig_value / (sig_value + k)
        return rogue


def filter_matrix(tmp, min_cells=10, min_genes=10):
    genes_per_cell = np.sum(tmp > 0, axis=0)
    cells_per_gene = np.sum(tmp > 0, axis=1)
    keep_genes = cells_per_gene > min_cells
    keep_cells = genes_per_cell > min_genes
    return tmp.loc[keep_genes, keep_cells]


def apply_rogue(
    expr, k, span=0.5, r=1, remove_outlier_n=2, mt_method="fdr_bh", pval_cutoff=0.05
):
    ent = entropy(expr, r=r)
    ent = entropy_fit(ent, span=span, mt_method=mt_method)
    if ent is None:
        return np.NaN
    ent = remove_outlier_cells(
        ent,
        expr,
        n=remove_outlier_n,
        span=span,
        r=r,
        mt_method=mt_method,
        pval_cutoff=pval_cutoff,
    )
    if ent is None:
        return np.NaN
    rogue = calculate_rogue(ent, k, cutoff=pval_cutoff)
    return rogue


def rogue_legacy(
    expr,
    labels,
    samples,
    platform=None,
    k=None,
    min_cluster_size=10,
    remove_outlier_n=2,
    span=0.5,
    r=1,
    filter_low=False,
    min_cells=10,
    min_genes=10,
    mt_method="fdr_bh",
    pval_cutoff=0.05,
    verbose=True,
):
    if k is None:
        if platform is None:
            raise TypeError(
                "Please provide a 'platform' argument or specify a 'k' value."
            )
        elif platform == "UMI":
            k = 45
        elif platform == "full-length":
            k = 500
        else:
            raise TypeError("'platform' must be one of 'UMI', 'full-length'.")
    result = pd.DataFrame(index=np.unique(samples), columns=np.unique(labels))
    for c in np.unique(labels):
        for s in np.unique(samples):
            # find cells that belong in this combination of sample/cluster
            keep = (samples == s) & (labels == c)
            # check if the cluster is too small to bother:
            if np.sum(keep) < min_cluster_size:
                if verbose:
                    print("Skipping sample {} for cluster {}".format(s, c))
                continue

            # define our temporary subset:
            tmp = expr.loc[:, keep]

            # check if we want to filter:
            if filter_low:
                if verbose:
                    print(
                        "Filtering out genes expressed in <{} cells and cells with <{} genes.".format(
                            min_cells, min_genes
                        )
                    )
                tmp = filter_matrix(tmp, min_cells=min_cells, min_genes=min_genes)

            # the actual calculations:
            result.loc[s, c] = apply_rogue(
                tmp,
                k,
                span=span,
                r=r,
                remove_outlier_n=remove_outlier_n,
                mt_method=mt_method,
                pval_cutoff=pval_cutoff,
            )
    return result


def _create_inputs(
    expr,
    labels,
    samples,
    min_cluster_size=10,
    filter_low=False,
    min_cells=10,
    min_genes=10,
    verbose=True,
):
    inputs = []
    for c in np.unique(labels):
        for s in np.unique(samples):
            # find cells that belong in this combination of sample/cluster
            keep = (samples == s) & (labels == c)
            # check if the cluster is too small to bother:
            if np.sum(keep) < min_cluster_size:
                if verbose:
                    print("Skipping sample {} for cluster {}".format(s, c))
                continue

            # define our temporary subset:
            tmp = expr.loc[:, keep]

            # check if we want to filter:
            if filter_low:
                if verbose:
                    print(
                        "Filtering out genes expressed in <{} cells and cells with <{} genes.".format(
                            min_cells, min_genes
                        )
                    )
                tmp = filter_matrix(tmp, min_cells=min_cells, min_genes=min_genes)
            inputs.append([c, s, tmp])
    return inputs


def rogue_atom(inputs, params):
    c, s, tmp = inputs
    span, r, remove_outlier_n, k, mt_method, pval_cutoff = params
    res = apply_rogue(
        tmp,
        k,
        span=span,
        r=r,
        remove_outlier_n=remove_outlier_n,
        mt_method=mt_method,
        pval_cutoff=pval_cutoff,
    )
    return c, s, res


# parallel version with multiprocessing/joblib. Def. room for improvement with cython here, but only
# invest if we truly need it.
def rogue(
    expr,
    labels,
    samples,
    platform=None,
    k=None,
    min_cluster_size=10,
    remove_outlier_n=2,
    span=0.5,
    r=1,
    filter_low=False,
    min_cells=10,
    min_genes=10,
    mt_method="fdr_bh",
    pval_cutoff=0.05,
    verbose=True,
    num_cores=None,
    pretty_output=True,
):
    # do some parameter checking first to save time from each iteration
    # first set k, our normalisation constant. In subsequent function calls we will only use k, not
    # the platform argument.
    if k is None:
        if platform is None:
            raise TypeError(
                "Please provide a 'platform' argument or specify a 'k' value."
            )
        elif platform == "UMI":
            k = 45
        elif platform == "full-length":
            k = 500
        else:
            raise TypeError("'platform' must be one of 'UMI', 'full-length'.")

    if num_cores is None:
        num_cores = multiprocessing.cpu_count()

    # make inputs and parameter list
    inputs = _create_inputs(
        expr,
        labels,
        samples,
        min_cluster_size=min_cluster_size,
        filter_low=filter_low,
        min_cells=min_cells,
        min_genes=min_genes,
        verbose=verbose,
    )
    parameters = [span, r, remove_outlier_n, k, mt_method, pval_cutoff]
    # dispatch to cores
    processed_list = Parallel(n_jobs=num_cores)(
        delayed(rogue_atom)(i, parameters) for i in inputs
    )

    if pretty_output:
        res = pd.DataFrame(columns=np.unique(labels), index=np.unique(samples))
        for r in processed_list:
            res.loc[r[1]][r[0]] = r[2]
        return res
    return processed_list


def roguer_than(scores, cutoff, strict=True):
    if strict:
        high_scores = np.mean(scores) > cutoff
        return np.where(high_scores)[0]
    else:
        high_scores = np.where(scores > cutoff)[1]
        return np.unique(high_scores)
